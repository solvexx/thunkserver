﻿using thunkserver.Models;
using thunkserver.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace thunkserver
{
    public static class SeedData
    {
        public static async Task InitializeAsync(IServiceProvider services)
        {
            await AddAdminUserAndClient(
                services.GetRequiredService<RoleManager<UserRoleEntity>>(),
                services.GetRequiredService<UserManager<UserEntity>>(),
                services.GetRequiredService<IClientService>()
                );
        }

        private static async Task AddAdminUserAndClient(
            RoleManager<UserRoleEntity> roleManager,
            UserManager<UserEntity> userManager,
            IClientService defaultClientService)
        {
            var dataExists = roleManager.Roles.Any() || userManager.Users.Any();
            if (dataExists)
            {
                return;
            }

            //Create an admin client
            var clientID = Guid.Parse("18AEB4BA-E630-4E7D-816F-C9AFA1703AAB");
            await defaultClientService.CreateClientAsync(clientID, "AdminClient");

            // Add the roles
            await roleManager.CreateAsync(new UserRoleEntity("Admin"));

            // Add an admin user
            var user = new UserEntity
            {
                Email = "admin@bodi.local",
                UserName = "admin@bodi.local",
                FirstName = "Admin",
                LastName = "Administrator",
                ClientId = clientID,
                CreatedAt = DateTimeOffset.UtcNow
            };

            await userManager.CreateAsync(user, "Supersecret123!!");

            // Put the user in the admin role
            await userManager.AddToRoleAsync(user, "Admin");
            await userManager.UpdateAsync(user);
        }
    }
}
