﻿using thunkserver.Infrastructure;
using thunkserver.Models;
using thunkserver.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using Microsoft.AspNetCore.Identity;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace thunkserver.Controllers
{
    [Route("/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly PagingOptions _defaultPagingOptions;
        //allows the checking of authorization service in methods rather than outside them in the [Authorize} decorator
        private readonly IAuthorizationService _authzService;
        //user manager to be able to access users and clients
        private UserManager<UserEntity> _userManager;
        private readonly ILoggerManager _logger;

        public UsersController(
            UserManager<UserEntity> userManager,
            IUserService userService,
            IOptions<PagingOptions> defaultPagingOptions,
            IAuthorizationService authorizationService,
            ILoggerManager logger)
        {
            _userManager = userManager;
            _userService = userService;
            _defaultPagingOptions = defaultPagingOptions.Value;
            _authzService = authorizationService;
            _logger = logger;
        }

        [HttpGet(Name = nameof(GetVisibleUsersAsync))]
        public async Task<ActionResult<PagedCollection<User>>> GetVisibleUsersAsync(
            [FromQuery] PagingOptions pagingOptions,
            [FromQuery] SortOptions<User, UserEntity> sortOptions,
            [FromQuery] SearchOptions<User, UserEntity> searchOptions)
        {
            pagingOptions.Offset = pagingOptions.Offset ?? _defaultPagingOptions.Offset;
            pagingOptions.Limit = pagingOptions.Limit ?? _defaultPagingOptions.Limit;

            var users = new PagedResults<User>
            {
                Items = Enumerable.Empty<User>()
            };

            if (User.Identity.IsAuthenticated)
            {
                var canSeeEveryone = await _authzService.AuthorizeAsync(
                    User, "ViewAllUsersPolicy");
                if (canSeeEveryone.Succeeded)
                {
                    users = await _userService.GetUsersAsync(
                        pagingOptions, sortOptions, searchOptions);
                }
                else
                {
                    //add the roles to the user as this is not returned from the UserEntity
                    System.Security.Claims.ClaimsPrincipal currentUser = User;
                    var curUser = await _userManager.GetUserAsync(currentUser);
                    var userRoles = await _userManager.GetRolesAsync(curUser);

                    var myself = await _userService.GetUserAsync(User, userRoles);
                    users.Items = new[] { myself };
                    users.TotalSize = 1;
                }
            }

            var collection = PagedCollection<User>.Create<UsersResponse>(
                Link.ToCollection(nameof(GetVisibleUsersAsync)),
                users.Items.ToArray(),
                users.TotalSize,
                pagingOptions);

            collection.Me = Link.To(nameof(GetMeAsync));
            collection.Register = FormMetadata.FromModel(
                new RegisterForm(),
                Link.ToForm(nameof(RegisterUserAsync), relations: Form.CreateRelation));

            return collection;
        }

        [Authorize]
        [HttpGet("me", Name = nameof(GetMeAsync))]
        public async Task<IActionResult> GetMeAsync()
        {
            if (User == null) return BadRequest();

            //add the roles to the user as this is not returned from the UserEntity
            System.Security.Claims.ClaimsPrincipal currentUser = this.User;
            var curUser = await _userManager.GetUserAsync(currentUser);
            var userRoles = await _userManager.GetRolesAsync(curUser);

            var user = await _userService.GetUserAsync(User, userRoles);
            if (user == null) return NotFound();

            return Ok(user);
        }

        [Authorize]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [HttpGet("{userId}", Name = nameof(GetUserByIdAsync))]
        public async Task<ActionResult<User>> GetUserByIdAsync(Guid userId)
        {
            var currentUserId = await _userService.GetUserIdAsync(User);
            if (currentUserId == null) return NotFound();

            if (currentUserId == userId)
            {
                //add the roles to the user as this is not returned from the UserEntity
                System.Security.Claims.ClaimsPrincipal currentUser = User;
                var curUser = await _userManager.GetUserAsync(currentUser);
                var userRoles = await _userManager.GetRolesAsync(curUser);

                var myself = await _userService.GetUserAsync(User, userRoles);
                return myself;
            }

            var canSeeEveryone = await _authzService.AuthorizeAsync(
                User, "ViewAllUsersPolicy");
            if (!canSeeEveryone.Succeeded) return NotFound();

            var user = await _userService.GetUserByIdAsync(userId);
            if (user == null) return NotFound();

            return user;
        }

        //CREATE USER
        //  {
        //	"firstName" : "Helen",
        //	"lastName": "Dematos",
        //	"email": "helen.dematos@alsico.co.uk",
        //	"password": "Alisco@1",
        //	"ClientID": "261AC2CF-DE08-401C-9A62-82CEF3DB79FA"
        //}
        // POST /users
        [HttpPost(Name = nameof(RegisterUserAsync))]
        [ProducesResponseType(400)]
        [ProducesResponseType(201)]
        public async Task<IActionResult> RegisterUserAsync(
            [FromBody] RegisterForm form)
        {
            var (succeeded, message) = await _userService.CreateUserAsync(form);
            if (succeeded) return Created(
                Url.Link(nameof(GetMeAsync), null),
                null);

            return BadRequest(new ApiError(_logger)
            {
                Message = "Registration failed.",
                Detail = message
            });
        }
    }
}
