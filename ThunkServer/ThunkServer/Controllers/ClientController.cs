﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using thunkserver.Infrastructure;
using thunkserver.Models;
using thunkserver.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json.Linq;

namespace thunkserver.Controllers
{

    [Route("/[Controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    public class ClientController : ControllerBase
    {
        //user manager to be able to access users and clients
        private UserManager<UserEntity> _userManager;
        private readonly IClientService _clientService;
        private readonly ILoggerManager _logger;

        public ClientController(
            IClientService clientService,
            UserManager<UserEntity> userManager,
            ILoggerManager logger)
        {
            _userManager = userManager;
            _clientService = clientService;
            _logger = logger;
        }

        //GET CLIENT INFO
        //GET /client
        [Authorize]
        [ProducesResponseType(200)]
        [HttpGet(Name = nameof(GetClientAsync))]
        public async Task<IActionResult> GetClientAsync()
        {
            _logger.LogInfo("ClientController_GetClientAsync");

            //get the clientID out of the current user
            System.Security.Claims.ClaimsPrincipal currentUser = this.User;
            var curUser = await _userManager.GetUserAsync(currentUser);

            //get the client for the current user
            var client = await _clientService.GetClientByIdAsync(curUser.ClientId);

            if (client == null) return NotFound();

            return Ok(client);
        }

        //GET AN INDIVIDUAL CLIENT USING ID
        //GET /client
        [Authorize]
        [ProducesResponseType(200)]
        [HttpGet("{clientid}", Name = nameof(GetClientByIDAsync))]
        public async Task<IActionResult> GetClientByIDAsync(
                Guid clientid)
        {
            _logger.LogInfo("ClientController_GetClientByIDAsync_clientid_" + clientid);

            //get the client for the current user
            var client = await _clientService.GetClientByIdAsync(clientid );

            if (client == null) return NotFound();

            return Ok(client);
        }

        //CREATE OR UPDATE CLIENT
        //Checks whether client exists already by checking for an optional GUID in the form and creates if not existing, or updates if present
        //POST /client
        //Example Data format
        //        {
        //	"ClientID": "288FDA62-B7D6-4092-AA65-2F8102795618",
        //	"Name": "Curves with Purpose",
        //	"DefaultUnits": "0",
        //    "mastermeasurements": [
        //        {
        //            "mmID": "3",
        //            "ToBeCollected": "True",
        //            "IncludeInResponse": "False"
        //        },
        //        {
        //            "mmID": "5",
        //            "ToBeCollected": "True",
        //            "IncludeInResponse": "False"
        //        },
        //        {
        //            "mmID": "6",
        //            "ToBeCollected": "True",
        //            "IncludeInResponse": "False"
        //        }   
        //    ]
        //}
        [Authorize]
        [ProducesResponseType(201)]
        [HttpPost(Name = nameof(CreateClientAsync))]
        public async Task<IActionResult> CreateClientAsync(
              [FromBody] JObject clientSource)
        {
            _logger.LogInfo("ClientController_CreateClientAsync_clientSource" + clientSource);

            // dynamic input from inbound JSON
            dynamic clientData = clientSource;

            string cID = (string)clientData.ClientID;

            Guid clientID = Guid.Parse(cID);

            //If there is no clientId create a new one
            if(clientID == Guid.Parse("00000000-0000-0000-0000-000000000000")) clientID = Guid.NewGuid();

            //test the units correspond to a value enum
            string dUnits = (string)clientData.DefaultUnits;
            Units defaultUnits = (Units)Enum.Parse(typeof(Units), dUnits);
            // the defaultUnits.ToString().Contains(",") check is necessary for enumerations marked with an [Flags] attribute
            if (!Enum.IsDefined(typeof(Units), defaultUnits) && !defaultUnits.ToString().Contains(","))
                return BadRequest(new ApiError(_logger)
                {
                    Message = $"{dUnits} is not an underlying value of the Units enumeration.",
                    Detail = ""
                });


            //var created = await _clientService.CreateClientAsync(clientID, (string) clientData.Name, defaultUnits, clientMMs, readOnlyGarmentMeasurements);
            //if (created < 1) return BadRequest(new ApiError(_logger).ApiErrorMsg(Constant.CouldNotCreateNewClient));

            //the second parameter values need to match the parameters for the method referrred to
            string returnLink = Url.Link(nameof(ClientController.GetClientByIDAsync), new { clientID });
            return Created(returnLink, null);
        }

        //DELETE A CLIENT
        [Authorize]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [HttpDelete("{ClientID}", Name = nameof(DeleteClientIdAsync))]
        public async Task<IActionResult> DeleteClientIdAsync(string clientid)
        {
            _logger.LogInfo("ClientController_DeleteClientIdAsync_" + clientid);

            //get the clientID out of the current user
            System.Security.Claims.ClaimsPrincipal currentUser = this.User;
            var curUser = await _userManager.GetUserAsync(currentUser);

            Guid clientID = Guid.Empty;
            var parseSuccess = Guid.TryParse(clientid, out clientID);

            if (parseSuccess)
            {
                await _clientService.DeleteClientAsynch(clientID);
                return NoContent();
            }
            else return NotFound();
        }
    }
}
