﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using thunkserver.Infrastructure;
using thunkserver.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace thunkserver.Controllers
{
    //use the name of the controller as the route name, instead of hard coding strings so in this case the route will be Body,as the Controller bit is ignored.
    [Route("/[Controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    public class RolesController : ControllerBase
    {
        private readonly RoleManager<UserRoleEntity> _roleManager;
        private readonly ILoggerManager _logger;
        public RolesController(RoleManager<UserRoleEntity> roleManager, ILoggerManager logger)
            {
                _roleManager = roleManager;
                _logger = logger;
        }

        //CREATES A NEW ROLE
        [Authorize]
        [HttpPost("{rolename}", Name = nameof(AddRole))]
        [ProducesResponseType(400)]
        [ProducesResponseType(200)]
        public async Task<IActionResult> AddRole(string rolename)
        {
            var success = await _roleManager.CreateAsync(new UserRoleEntity(rolename));
            if (success != null) return Ok();
            else return BadRequest(new ApiError(_logger).ApiErrorMsg(Constant.RoleCreationFailure));
        }
    }
}
