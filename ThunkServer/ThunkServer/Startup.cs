using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using thunkserver.Filters;
using thunkserver.Infrastructure;
using thunkserver.Models;
using thunkserver.Services;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.AspNetCore.Identity;
using AspNet.Security.OpenIdConnect.Primitives;
using OpenIddict.Validation;
using Microsoft.EntityFrameworkCore;
using NLog;

namespace thunkserver
{
    public class Startup
    {
        private readonly int? _httpsPort;
        public readonly IWebHostEnvironment hostingEnvironment;
        public IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            LogManager.LoadConfiguration(String.Concat(env.ContentRootPath, "/nlog.config"));
            Configuration = configuration;

            //var builder = new ConfigurationBuilder()
            //    .SetBasePath(env.ContentRootPath)
            //    .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
            //    .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
            //    .AddEnvironmentVariables();
            //Configuration = builder.Build();

            //// Get the HTTPS port (only in development) read from launchsettings.json
            //if (env.IsDevelopment())
            //{
            //    var launchJsonConfig = new ConfigurationBuilder()
            //        .SetBasePath(env.ContentRootPath)
            //        .AddJsonFile("Properties\\launchSettings.json")
            //        .Build();
            //    _httpsPort = launchJsonConfig.GetValue<int>("iisSettings:iisExpress:sslPort");
            //}
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<ILoggerManager, DefaultLoggerService>();

            services.Configure<PagingOptions>(
                Configuration.GetSection("DefaultPagingOptions"));

            services.AddScoped<IUserService, DefaultUserService>();


            services.AddScoped<IUserService, DefaultUserService>();
            services.AddScoped<IClientService, DefaultClientService>();

            services.AddDbContext<ThunkServerContext>(
                options =>
                {
                    options.UseOpenIddict<Guid>();
                    options.UseSqlServer(Configuration.GetConnectionString("ThunkServer"));
                });



            // Add OpenIddict services
            services.AddOpenIddict()
                .AddCore(options =>
                {
                    options.UseEntityFrameworkCore()
                        .UseDbContext<ThunkServerContext>()
                        .ReplaceDefaultEntities<Guid>();
                })
                .AddServer(options =>
                {
                    options.UseMvc();

                    options.EnableTokenEndpoint("/token");

                    options.AllowPasswordFlow();
                    options.AcceptAnonymousClients();
                })
                .AddValidation();

            // ASP.NET Core Identity should use the same claim names as OpenIddict
            services.Configure<IdentityOptions>(options =>
            {
                options.ClaimsIdentity.UserNameClaimType = OpenIdConnectConstants.Claims.Name;
                options.ClaimsIdentity.UserIdClaimType = OpenIdConnectConstants.Claims.Subject;
                options.ClaimsIdentity.RoleClaimType = OpenIdConnectConstants.Claims.Role;
            });

            services.AddAuthentication(options =>
            {
                options.DefaultScheme = OpenIddictValidationDefaults.AuthenticationScheme;
            });

            // Add ASP.NET Core Identity
            AddIdentityCoreServices(services);

            services
                .AddMvc(options =>
                {
                    options.CacheProfiles.Add("Static", new CacheProfile { Duration = 60, Location = ResponseCacheLocation.None, NoStore = true});
                    options.CacheProfiles.Add("Collection", new CacheProfile { Duration = 60, Location = ResponseCacheLocation.None, NoStore = true });
                    options.CacheProfiles.Add("Resource", new CacheProfile { Duration = 180, Location = ResponseCacheLocation.None, NoStore = true });
                    options.Filters.Add<JsonExceptionFilter>();
                    options.Filters.Add<RequireHttpsOrCloseAttribute>();
                    options.Filters.Add<LinkRewritingFilter>();

                    options.EnableEndpointRouting = false;
                });

            //set URLs to lowercase
            services.AddRouting(opt => opt.LowercaseUrls = true);

            services.AddApiVersioning(options =>
            {
                options.DefaultApiVersion = new ApiVersion(1, 0);
                options.ApiVersionReader
                    = new MediaTypeApiVersionReader();
                options.AssumeDefaultVersionWhenUnspecified = true;
                options.ReportApiVersions = true;
                options.ApiVersionSelector
                     = new CurrentImplementationApiVersionSelector(options);
            });

            services.AddAutoMapper(typeof(MappingProfile));

            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.InvalidModelStateResponseFactory = context =>
                {
                    var errorResponse = new ApiError(null).ApiErrorModelState(context.ModelState);
                    return new BadRequestObjectResult(errorResponse);
                };
            });


            services.AddAuthorization();

            //controls how browsers can access the API
            //for security should add whitelists per client, although this ties us to any updates a client does
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAny",
                    policy => policy
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowAnyOrigin()
                    .WithExposedHeaders("Location")
                );
            });

            services.AddControllers();

            services.AddControllers().AddNewtonsoftJson();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ThunkServerContext context)
        {
            //context.Database.Migrate();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            //Accept All HTTP Request Methods from all origins
            app.UseCors("AllowAny");

            ////Removed and instead return a 400 as a bad request, if someone tries to send anything over HTTP
            ////uses the RequireHttpsOrCloseAttribute filter
            ////app.UseHttpsRedirection();

            app.UseAuthentication();

            app.UseMvc();
        }

        private static void AddIdentityCoreServices(IServiceCollection services)
        {
            var builder = services.AddIdentityCore<UserEntity>();
            builder = new IdentityBuilder(
                builder.UserType,
                typeof(UserRoleEntity),
                builder.Services);

            builder.AddRoles<UserRoleEntity>()
                .AddEntityFrameworkStores<ThunkServerContext>()
                .AddDefaultTokenProviders()
                .AddSignInManager<SignInManager<UserEntity>>();
        }
    }
}
