﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace thunkserver.Infrastructure
{
    public static partial class CommonHelper
    {
        //converts first char of a string to upper case.
        public static string FirstCharToUpper(this string input) =>
        input switch
        {
            null => throw new ArgumentNullException(nameof(input)),
            "" => throw new ArgumentException($"{nameof(input)} cannot be empty", nameof(input)),
            _ => input.First().ToString().ToUpper() + input.Substring(1)
        };


        /// <summary>
        /// Calculates the age of a person based on their date of birth
        /// </summary>
        /// <param name="dateOfBirth">Date Of Birth</param>
        /// <returns></returns>
        public static int CalculateAge(DateTime dateOfBirth)
        {
            var today = DateTime.Today;
            var age = today.Year - dateOfBirth.Year;
            if (dateOfBirth > today.AddYears(-age)) age--;
            return age;
        }

        public static void EnsurePathExists(string path)
        {
            // ... Set to folder path we must ensure exists.
            try
            {
                // ... If the directory doesn't exist, create it.
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
            }
            catch (Exception)
            {
                //ToDo generic error handling reporting
                // Fail silently.
            }
        }

        //public static decimal ToInches(decimal centimeters)
        //{
        //    return Decimal.Round(centimeters * (decimal)0.393700787, 1);
        //}

        //public static decimal ToKilos(decimal lb)
        //{
        //    return Decimal.Round(lb * 0.453592m, 3);
        //}

        //public static decimal ToPounds(decimal kg)
        //{
        //    return Decimal.Round(kg / 0.453592m, 0);
        //}

        //public static decimal ToCentimeters(decimal inches)
        //{
        //    return Decimal.Round(inches * (decimal)2.54, 1);
        //}

        //public static decimal ToCentimeters(decimal inches, string units)
        //{
        //    return Decimal.Round(inches * (decimal)2.54, 1);
        //}

        /// <summary>
        /// Converts feet plus inches to metric
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static decimal ImperialToMetric(this string val)
        {
            /*
             * With these inputst we want to total inches.
             * to do this we want to standardize the feet designator to 'f' 
             * and remove the inch designator altogether.
                6 inches
                6in
                6”
                4 feet 2 inches
                4’2”
                4 ‘ 2 “
                3 feet
                3’
                3 ‘
                3ft
                3ft10in
                3ft 13in (should convert to 4’1”) ...no, should convert to 49 inches, then to metric.
             */

            //make the input lower case and remove blanks:
            val = val.ToLower().Replace(" ", string.Empty);

            //make all of the 'normal' feet designators to "ft"
            string S =
                val.Replace("\'", "f")
                    .Replace("feet", "f")
                    .Replace("ft", "f")
                    .Replace("foot", "f")
                    .Replace("‘", "f")
                    .Replace("’", "f");

            //and remove any inch designator
            S =
                S.Replace("\"", string.Empty)
                    .Replace("inches", string.Empty)
                    .Replace("inch", string.Empty)
                    .Replace("in", string.Empty)
                    .Replace("“", string.Empty)
                    .Replace("”", string.Empty);

            //finally we have to be certain we have a number of feet, even if that number is zero
            S = S.IndexOf('f') > 0 ? S : "0f" + S;

            //now, any of the inputs above will have been converted to a string 
            //that looks like 4 feet 2 inches => 4f2

            var values = S.Split('f');

            decimal inches = 0;
            //as long as this produces one or two values we are 'on track' 
            if (values.Length < 3)
            {
                for (var i = 0; i < values.Length; i++)
                {
                    inches += values[i] != null && values[i] != string.Empty
                        ? decimal.Parse(values[i], CultureInfo.InvariantCulture) * (i == 0 ? 12 : 1)
                        : 0;
                }
            }

            //now inches = total number of inches in the input string.
            var result = inches * 2.54m;
            return result;
        }

        private static readonly string[] OrderedTextSizes =
        {
            "XXXS", "XXS", "XS", "S", "M", "L", "XL", "XXL", "XXXL", "EECH", "ECH", "CH", "M", "G", "EG", "EEG", "EEEG"
        };

        public static List<string> GetOrderedSizes(List<string> suggestedSizes)
        {
            var finalSizes = new List<string>();

            foreach (var size in OrderedTextSizes)
            {
                if (suggestedSizes.Contains(size))
                {
                    suggestedSizes.Remove(size);
                    finalSizes.Add(size);
                }
            }

            var orderedSizes = new List<int>();
            foreach (var size in suggestedSizes.ToList())
            {
                var valueNumeric = 0;
                if (int.TryParse(size, out valueNumeric))
                {
                    orderedSizes.Add(valueNumeric);
                    suggestedSizes.Remove(size);
                }

            }
            orderedSizes.Sort();

            finalSizes.AddRange(orderedSizes.Select(x => x.ToString(CultureInfo.InvariantCulture)));
            finalSizes.AddRange(suggestedSizes);
            return finalSizes;
        }
    }

    //compares strings ignoring white space.
    public class StringCompIgnoreWhiteSpace : IEqualityComparer<string>
    {
        public bool Equals(string strx, string stry)
        {
            if (strx == null) //stry may contain only whitespace
                return string.IsNullOrWhiteSpace(stry);

            else if (stry == null) //strx may contain only whitespace
                return string.IsNullOrWhiteSpace(strx);

            int ix = 0, iy = 0;
            for (; ix < strx.Length && iy < stry.Length; ix++, iy++)
            {
                char chx = strx[ix];
                char chy = stry[iy];

                //ignore whitespace in strx
                while (char.IsWhiteSpace(chx) && ix < strx.Length)
                {
                    ix++;
                    chx = strx[ix];
                }

                //ignore whitespace in stry
                while (char.IsWhiteSpace(chy) && iy < stry.Length)
                {
                    iy++;
                    chy = stry[iy];
                }

                if (ix == strx.Length && iy != stry.Length)
                { //end of strx, so check if the rest of stry is whitespace
                    for (int iiy = iy + 1; iiy < stry.Length; iiy++)
                    {
                        if (!char.IsWhiteSpace(stry[iiy]))
                            return false;
                    }
                    return true;
                }

                if (ix != strx.Length && iy == stry.Length)
                { //end of stry, so check if the rest of strx is whitespace
                    for (int iix = ix + 1; iix < strx.Length; iix++)
                    {
                        if (!char.IsWhiteSpace(strx[iix]))
                            return false;
                    }
                    return true;
                }

                //The current chars are not whitespace, so check that they're equal (case-insensitive)
                //Remove the following two lines to make the comparison case-sensitive.
                chx = char.ToLowerInvariant(chx);
                chy = char.ToLowerInvariant(chy);

                if (chx != chy)
                    return false;
            }

            //If strx has more chars than stry
            for (; ix < strx.Length; ix++)
            {
                if (!char.IsWhiteSpace(strx[ix]))
                    return false;
            }

            //If stry has more chars than strx
            for (; iy < stry.Length; iy++)
            {
                if (!char.IsWhiteSpace(stry[iy]))
                    return false;
            }

            return true;
        }

        public int GetHashCode(string obj)
        {
            if (obj == null)
                return 0;

            int hash = 17;
            unchecked // Overflow is fine, just wrap
            {
                for (int i = 0; i < obj.Length; i++)
                {
                    char ch = obj[i];
                    if (!char.IsWhiteSpace(ch))
                        //use this line for case-insensitivity
                        hash = hash * 23 + char.ToLowerInvariant(ch).GetHashCode();

                    //use this line for case-sensitivity
                    //hash = hash * 23 + ch.GetHashCode();
                }
            }
            return hash;
        }
    }
}
