﻿using System;
using System.Linq.Expressions;

namespace thunkserver.Infrastructure
{
    public class BooleanSearchExpressionProvider : DefaultSearchExpressionProvider
    {
        public override ConstantExpression GetValue(string input)
        {
            if (!bool.TryParse(input, out var value))
                throw new ArgumentException("Invalid search value.");

            return Expression.Constant(value);
        }
    }
}