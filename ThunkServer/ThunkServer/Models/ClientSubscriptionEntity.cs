﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace thunkserver.Models
{
    public class ClientSubscriptionEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ClientSubscriptionId { get; set; }

        //reference back to the subscribing client
        public Guid ClientId { get; set; }
        public virtual ClientEntity Client { get; set; }

        public DateTime? PurchaseDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public int SuggestionMonthlyLimit { get; set; }
        public int PhotoUploadLimit { get; set; }
    }
}
