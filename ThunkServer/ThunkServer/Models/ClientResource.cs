﻿using thunkserver.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace thunkserver.Models
{
    //Representas a BodiMe Client as stored in the DB

    public class ClientResource : Resource
    {
        [StringLength(100)]
        public string Name { get; set; }

        //whether the client is no longer active
        public bool Deleted { get; set; }

        public Units DefaultUnits { get; set; }

        public DateTime? LastSynch { get; set; }

        public bool ReadOnlyGarmentMeasurements { get; set; }
    }
}
