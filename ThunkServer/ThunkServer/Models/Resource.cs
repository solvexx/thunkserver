﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace thunkserver.Models
{
    public abstract class Resource : Link
    {
        [JsonIgnore] //this is because ion standard says that just a link is self referential 
        public Link Self { get; set; }
    }
}
