﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace thunkserver.Models
{
    //Representas a BodiMe Client as stored in the DB

    public class ClientEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid ClientID { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        //whether the client is no longer active
        public bool Deleted { get; set; }
    }
}
