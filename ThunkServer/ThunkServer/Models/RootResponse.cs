﻿using thunkserver.Infrastructure;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace thunkserver.Models
{
    public class RootResponse : Resource, IEtaggable
    {
        public Link NameOfBodyRoute { get; set; }

        public Link NameOfGarmentRoute { get; set; }

        public Link NameOfUsersRoute { get; set; }

        public Link NameOfSuggestionRoute { get; set; }

        public Form Token { get; set; }

        public string GetEtag()
        {
            var serialized = JsonConvert.SerializeObject(this);
            return Md5Hash.ForString(serialized);
        }
    }
}
