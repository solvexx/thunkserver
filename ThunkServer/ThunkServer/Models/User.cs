﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace thunkserver.Models
{
    public class User : Resource
    {
        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public IList<string> UserRoles { get; set; }

        public DateTimeOffset CreatedAt { get; set; }
    }
}
