﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace thunkserver.Models
{
    public sealed class Constant
    {
        public static readonly string UKJEANS = "ukjeans";
        public static readonly string UKSHIRT = "ukshirt";
        public static readonly string UKBRA = "ukbra";
        public static readonly string UKBRACUP = "ukbracup";

        public static readonly string JEANS = "jeans";
        public static readonly string SHIRT = "shirt";
        public static readonly string BRA = "bra";
        public static readonly string BRACUP = "bracup";

        public static readonly string POSTURE = "posture";
        public static readonly string SHOULDERSLOPE = "shoulderslope";
        public static readonly string CHESTSIZE = "chestsize";
        public static readonly string STOMACH = "stomach";
        public static readonly string BUTTOCKSSIZE = "buttockssize";
        public static readonly string THIGHSHAPE = "thighShape";

        public static readonly string SIZECHARTCOUNTRY = "sizechartcountry"; //parameter within body creation json indicating which country size chart processing is to be applied.

        public static readonly string WEIGHTSTONES = "weightStones"; //special API variable names 
        public static readonly string HEIGHTFEET = "heightFeet";//special API variable names 

        //API Return messages
        public static readonly string NoSubscription = "No valid service subscription found"; //when the client does not have a valid subsription
        public static readonly string NoGarmentRecommendationError = "We don't have a recommendation for this garment"; //Garment not found
        public static readonly string NoBodyRecommendationError = "We don't have a recommendation for this person"; //Body not found
        public static readonly string DoesNotFitRecommendationError = "Sorry, we cannot find a size for you"; //Garment does not have a defined size that fits the person 
        public static readonly string NoEquivalentGarmentBodyMeasurementError = "We don't have a recommendation for this product"; //Data config error the garment and body measurements don't match so can't be compared
        public static readonly string DoesNotFitResponse = "Does not fit"; //garment does not fit the body
        public static readonly string DoesNotFitLengthResponse = "Special Length"; //garment does not fit the body
        public static readonly string TestBatchCreationFailure = "Could not create TestBatchSuggestion"; //when a test batch can't be created
        public static readonly string TestBatchNoGarmentsFailure = "No garments selected"; //when a test batch creation failed as no garments included in the list
        public static readonly string TestBatchNoBodiesFailure = "No people selected";//when a test batch creation failed as no people included in the list
        public static readonly string RoleCreationFailure = "Could not create Role";
        public static readonly string DoesNotFitSmallSizeToleranceResponse = "Does not fit, smallest garment too large"; //garment does not fit the body as the body is outside the small size tolerance
        public static readonly string BodyMissingHeightOrWeight = "Person does not have height or weight measuremnent";
        public static readonly string CouldNotCreateNewClient = "Could not create new client or it exists already and data received matches existing record";
        public static readonly string CouldNotCreateNewClientSources = "Could not create new client sources";

        public const decimal METRICTOIMPERIALRATIO = 2.54M;
        public const decimal KILOSPOUNDSRATIO = 0.4536m;
    }

    public enum Status
    {
        Pending = 0,
        Complete = 1,
        Disabled = 2
    }
    public enum Gender
    {
        Male = 0,
        Female = 1,
        Unisex = 2
    }

    //How a set of measurements were taken
    //Used to map specific measuring system names to system names
    //via lookup class/table
    //ToDo WRITE A TEST TO CHECK THIS MATCHES THE DB TABLE OR AUTOMATICALLY POPULATE IT
    public enum MeasurementSource

    {
        System = 0,
        Garment = 1,
        DefaultBodyManualMeasurementsService = 2,
        Styku1 = 3,
        Styku2 = 4,
        SizeStream1 = 5,
        SizeStream2 = 6,
        SOMA1 = 7,
        DefaultBespokifyService = 8,
        DefaultBodyDerivedMeasurementsService = 10,
        DefaultBodyHeightWeightBustThighHipWaistbandAlgoService = 11,
        DefaultBodyUKSizeChartService = 12,
        DefaultBodyItalySizeChartService = 13,
        DefaultBodyFranceSizeChartService = 14,
        DefaultBodyUSASizeChartService = 15,
        DefaultBodyEUSizeChartService = 16,
        DefaultBodyMLMeasurementsService = 17
    }

    //used for all measurements except height and weight which have 3 and are handled specifically
    public enum Units
    {
        Metric = 0,
        Imperial = 1, 
    }

    public enum HeightUnits
    {
        CMS = 0,
        Inches = 1,
        FeetandInches = 2
    }

    public enum WeightUnits
    {
        Kgs = 0,
        Pounds = 1,
        StonesandPounds = 2
    }

    public enum DisplayMeasurement
    {
        Yes = 0,
        No = 1
    }

    public static class GlobalVar
    {
        public const string roleAdmin  = "Admin";
        public const string roleClient = "Client";
    }

    public enum Bespokifyposture
    {
        veryUpright = 0,
        upright = 1,
        regular = 2,
        curved = 3,
        veryCurved = 4
    }

    //WARNING THESE ENUMS ARE REVERSED VERSUS BESPOKIFY
    //i.e. Deg11 should by 11Deg as expected by the API
    public enum BespokifyshoulderSlope
    {
        Deg11 = 0,
        Deg14 = 1,
        Deg17 = 2,
        regular = 3,
        Deg23 = 4,
        Deg26 = 5,
        Deg29 = 6
    }

    public enum BespokifychestSize
    {
        regular = 0,
        slightlyLarge = 1,
        large = 2
    }

    public enum Bespokifystomach
    {
        regular = 0,
        slightlyLarge = 1,
        large = 2
    }

    public enum BespokifybuttocksSize
    {
        flat = 0,
        slightlyFlat = 1,
        regular = 2,
        slightlyLarge = 3,
        large = 4,
    }

    public enum BespokifythighShape
    {
        regular = 0,
        muscular = 1,
        veryMusular = 2
    }
}
