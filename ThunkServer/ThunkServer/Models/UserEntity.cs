﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace thunkserver.Models
{
    public class UserEntity : IdentityUser<Guid>
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        //reference back to the Client the user represents
        public Guid ClientId { get; set; }
        public virtual ClientEntity Client { get; set; }

        public DateTimeOffset CreatedAt { get; set; }
    }
}
