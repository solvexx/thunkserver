﻿using thunkserver.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;

namespace thunkserver
{

    public class ThunkServerContext : IdentityDbContext<UserEntity, UserRoleEntity, Guid>
    {
        public ThunkServerContext(DbContextOptions options)
            : base(options) { }

        public virtual DbSet<ClientEntity> clientEntity { get; set; }

        public virtual DbSet<ClientSubscriptionEntity> clientSubscriptionEntity { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Configure domain classes using modelBuilder here
            base.OnModelCreating(modelBuilder);


        }
    }

    //public class TemporaryDbContextFactory : IDesignTimeDbContextFactory<ThunkServerContext>
    //{
    //    //////// 
    //    public ThunkServerContext CreateDbContext(string[] args)
    //    {
    //        var builder = new DbContextOptionsBuilder<ThunkServerContext>();
    //        IConfigurationRoot configuration = new ConfigurationBuilder()
    //           //.SetBasePath(IHostingEnvironment. env.ContentRootPath)
    //          //.SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
    //          .AddJsonFile("C:/Users/Keith/documents/visual studio 2017/Projects/bodimecore/bodimecore/appsettings.Development.json")
    //          //.AddJsonFile("appsettings.json")
    //          .Build();

    //        builder.UseSqlServer(configuration.GetConnectionString("BodimeCore"));
    //        return new ThunkServerContext(builder.Options);
    //    }
    //}
}

