﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using AutoMapper.QueryableExtensions;
using System.Security.Claims;
using AutoMapper;
using thunkserver.Models;
using System.Collections.Generic;

namespace thunkserver.Services
{
    public class DefaultUserService : IUserService
    {
        private readonly UserManager<UserEntity> _userManager;
        private readonly IConfigurationProvider _mappingConfiguration;
        private readonly ThunkServerContext _context;

        public DefaultUserService(ThunkServerContext context, UserManager<UserEntity> userManager, IConfigurationProvider mappingConfiguration)
        {
            _context = context;
            _userManager = userManager;
            _mappingConfiguration = mappingConfiguration;
        }

        //CREATE A USER
        //Assumes roles in in all CAPS (ToDo)
        public async Task<(bool Succeeded, string Error)> CreateUserAsync(RegisterForm form)
        {
            var entity = new UserEntity
            {
                Email = form.Email,
                UserName = form.Email,
                FirstName = form.FirstName,
                LastName = form.LastName,
                ClientId = Guid.Parse(form.ClientID),
                CreatedAt = DateTimeOffset.UtcNow
            };

            //create the user in the database
            var result = await _userManager.CreateAsync(entity, form.Password);
            if (!result.Succeeded)
            {
                var firstError = result.Errors.FirstOrDefault()?.Description;
                //c# 7 tuple
                return (false, firstError);
            }

            ////add the role to the user
            //if(form.Role != ""){
            //    //get the role ID based on the name
            //    var role = await _context.Roles.SingleOrDefaultAsync(r => r.NormalizedName == form.Role);
            //    if(role != null)
            //    {
            //        var user = _context.Users.SingleOrDefaultAsync(u => u.)
            //        var newUserRole = new UserRoleEntity()
            //        var roleUserEntity = _context.UserRoles.Add()
            //    }
            //}

            return (true, null);
        }

        //GET A USER
        public async Task<User> GetUserAsync(ClaimsPrincipal user, IList<string> userRoles)
        {
            var entity = await _userManager.GetUserAsync(user);

            var mapper = _mappingConfiguration.CreateMapper();

            var theUser = mapper.Map<User>(entity);
            theUser.UserRoles = new List<string>().ToList();

            //add the roles as this is not in the userEntity and therefore not mapped by mapper.
            foreach (var role in userRoles)
            {
                theUser.UserRoles.Add(role);
            }

            return theUser;
        }

        //GET A USER BY ID
        //Assumes the userRoles provided are those for the userId
        public async Task<User> GetUserByIdAsync(Guid userId)
        {
            var user = await _userManager.Users
                .SingleOrDefaultAsync(x => x.Id == userId );

            var mapper = _mappingConfiguration.CreateMapper();

            var theUser = mapper.Map<User>(user);

            //add the roles as this is not in the userEntity and therefore not mapped by mapper.
            //foreach (var role in userRoles)
            //{
            //    theUser.UserRoles.Add(role);
            //}

            return theUser;
        }

        public async Task<Guid?> GetUserIdAsync(ClaimsPrincipal principal)
        {
            var user = await _userManager.GetUserAsync(principal);
            if (user == null) return null;

            return user.Id;
        }

        public async Task<PagedResults<User>> GetUsersAsync(
            PagingOptions pagingOptions,
            SortOptions<User, UserEntity> sortOptions,
            SearchOptions<User, UserEntity> searchOptions)
        {
            IQueryable<UserEntity> query = _userManager.Users;
            query = searchOptions.Apply(query);
            query = sortOptions.Apply(query);

            var size = await query.CountAsync();

            var items = await query
                .Skip(pagingOptions.Offset.Value)
                .Take(pagingOptions.Limit.Value)
                .ProjectTo<User>(_mappingConfiguration)
                .ToArrayAsync();

            return new PagedResults<User>
            {
                Items = items,
                TotalSize = size
            };
        }
    }
}
