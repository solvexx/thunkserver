﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using thunkserver.Models;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using Newtonsoft.Json.Linq;

namespace thunkserver.Services
{
    public class DefaultClientService : IClientService
    {
        private readonly ThunkServerContext _context;
        private readonly IWebHostEnvironment _hostingEnvironment; //needed to locate the .josn file
        private readonly IConfigurationProvider _mappingConfiguration;

        public DefaultClientService(ThunkServerContext context, IWebHostEnvironment hostingEnvironment, IConfigurationProvider mappingConfiguration)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
            _mappingConfiguration = mappingConfiguration;
        }

        //CREATE CLIENT WITHOUT MEASURMENTS
        //Used in the empty database procedure, also sets up a client subscription with high volumes and end date 10 years away.
        public async Task<int> CreateClientAsync(Guid clientID, string name)
        {
            //check if client exists
            var client = await _context.clientEntity.SingleOrDefaultAsync(c => c.ClientID == clientID);
            if (client != null) return 0;

            var newClient = _context.clientEntity.Add(new ClientEntity
            {
                ClientID = clientID,
                Name = name,
                Deleted = false
            });

            //add a subscription with default large values
            var newClientSubscription = _context.clientSubscriptionEntity.Add(new ClientSubscriptionEntity
            {
                ClientId = clientID,
                PurchaseDate = DateTime.Now,
                ExpirationDate = DateTime.Now.AddYears(10),
                SuggestionMonthlyLimit = 100000,
                PhotoUploadLimit = 100000
            });

            var created = await _context.SaveChangesAsync();

            if (created < 1) throw new InvalidOperationException("Could not create client");

            return created;
        }

        //GET A CLIENT RESOURCE
        public async Task<ClientResource> GetClientByIdAsync(Guid clientID)
        {
            var entity = await _context.clientEntity.SingleOrDefaultAsync(c => c.ClientID == clientID);

            if (entity == null) return null;

            var mapper = _mappingConfiguration.CreateMapper();

            var resource = mapper.Map<ClientResource>(entity);

            return resource;
        }

        //GET A CLIENT ENTITY
        public async Task<ClientEntity> GetClientEntityByIdAsync(Guid clientID)
        {
            var entity = await _context.clientEntity.SingleOrDefaultAsync(c => c.ClientID == clientID);

            if (entity == null) return null;

            return entity;
        }

        //GET CLIENT SUBSCRIPTION
        //searches through all subscriptions and returns the first valid one for the client e.g. started by not expired
        public async Task<ClientSubscriptionEntity> GetClientSubscriptionAsync(Guid clientId)
        {
            var entity = await _context.clientSubscriptionEntity.SingleOrDefaultAsync(c => c.ClientId == clientId && c.PurchaseDate < DateTime.Now && c.ExpirationDate > DateTime.Now);
            if (entity == null) return null;
            return entity;
        }

        //DELETE CLIENT
        public async Task DeleteClientAsynch(Guid clientID)
        {
            var client = await _context.clientEntity.SingleOrDefaultAsync(b => b.ClientID == clientID);
            if (client == null) return;

            _context.clientEntity.Remove(client);
            await _context.SaveChangesAsync();

            return;
        }

    }
}
