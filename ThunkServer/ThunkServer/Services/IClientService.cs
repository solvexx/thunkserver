﻿using thunkserver.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace thunkserver.Services
{
    public interface IClientService
    {
        Task<int> CreateClientAsync(Guid clientID, string name);
         Task<ClientResource> GetClientByIdAsync(Guid ClientId);
        Task<ClientEntity> GetClientEntityByIdAsync(Guid ClientId);
        Task<ClientSubscriptionEntity> GetClientSubscriptionAsync(Guid ClientId);

        Task DeleteClientAsynch(Guid clientID);
    }
}
;